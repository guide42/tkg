import App from "./game/App.svelte"

export default new App({
  target: document.body,
  props: {
    contractAddress: "0x5b644e513614E0aFD71f91b457d08D4C3b8856B4",
    chainName: "rinkeby",
    chainId: "0x4",
    eventId: "4510"
  }
})
