import { writable } from "svelte/store"

function createEthereumStore() {
  const provider = window.ethereum

  const { subscribe, update } = writable({
    provider,
    connected: false,
    account: null,
    chainId: "0x0"
  })

  const onChainId = chainId => update(prevState => ({
    ...prevState,
    chainId
  }))

  const onAccounts = accounts => update(prevState => ({
    ...prevState,
    connected: accounts.length > 0,
    account: accounts.length > 0 ? accounts[0] : null
  }))

  function listen() {
    provider.on("accountsChanged", onAccounts)
    provider.on("chainChanged", onChainId)
    provider.request({ method: "eth_chainId" }).then(onChainId)
  }

  function connect() {
    provider.request({ method: "eth_requestAccounts" }).then(onAccounts)
  }

  function disconnect() {
    update(prevState => ({
      ...prevState,
      connected: false,
      account: null
    }))
  }

  return {
    subscribe,
    listen,
    connect,
    disconnect
  }
}

export const ethereum = createEthereumStore()
