import { Interface } from "ethers/lib.esm/utils"

import TKGv2 from "../abi/TKGv2.json"

export const TKG = new Interface(TKGv2)
