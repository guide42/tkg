import svelte from "rollup-plugin-svelte"
import css from "rollup-plugin-css-only"
import json from "@rollup/plugin-json"
import { nodeResolve } from "@rollup/plugin-node-resolve"
import commonjs from "@rollup/plugin-commonjs"
import { terser } from "rollup-plugin-terser"
import serve from "rollup-plugin-serve"

const IsDevelopment = process.env.APP_ENV === "development"

export default {
  input: "app/main.js",
  output: {
    sourcemap: true,
    format: "iife",
    name: "tkg",
    file: "public/dist/tkg.js"
  },
  cache: IsDevelopment,
  plugins: [
    svelte({
      compilerOptions: {
        dev: IsDevelopment
      }
    }),
    css({
      output: "tkg.css"
    }),
    json({
      compact: true
    }),
    nodeResolve({
      browser: true,
      mainFields: ["browser"],
      dedupe: ["svelte"]
    }),
    commonjs({
      sourceMap: IsDevelopment
    }),
    terser(),
    IsDevelopment && serve({
      open: true,
      contentBase: "public"
    })
  ],
  watch: {
    clearScreen: false
  }
}
