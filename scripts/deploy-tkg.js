async function main() {
  const TKG = await hre.ethers.getContractFactory("TKGv2")
  const tkg = await TKG.deploy("1000000000000000000", "0x22c1f6050e56d2876009903609a2cc3fef83b415", "4510")

  await tkg.deployed()

  console.log("TKG Contract Address = ", tkg.address)
}

main().
then(() => {
  process.exit(0)
}).
catch(err => {
  console.error(err)
  process.exit(1)
})
