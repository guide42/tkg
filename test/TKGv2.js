const { expect } = require("chai")
const { smockit } = require("@eth-optimism/smock")

describe("TKGv2", () => {
  let tkg, creator, player0, poapMock

  beforeEach(async () => {
    ;[
      creator,
      player0
    ] = await hre.ethers.getSigners()

    const TKG = await hre.ethers.getContractFactory("TKGv2")

    const Poap = await hre.ethers.getContractAt("Poap", "0x22c1f6050e56d2876009903609a2cc3fef83b415")

    poapMock = await smockit(Poap)

    tkg = await TKG.deploy("1000000000000000000", poapMock.wallet.address, "4510")

    await tkg.deployed()
  })

  describe("claim()", () => {

    it("requires that sender has a POAP for event 4510", async () => {
      expect(tkg.connect(player0).claim("1049")).to.be.revertedWith("Hold the correct POAP")
    })

    it("first time should emit Claimed event", async () => {
      poapMock.smocked.tokenEvent.will.return.with("4510")
      expect(await tkg.connect(player0).claim("607748")).to.emit(tkg, "Claimed").withArgs(player0.address, "4510", "607748")
      expect(poapMock.smocked.tokenEvent.calls.length).to.be.equal(1)
    })

    it("second time should do nothing", async () => {
      poapMock.smocked.tokenEvent.will.return.with("4510")
      await tkg.connect(player0).claim("607748") // first time
      expect(await tkg.connect(player0).claim("607748")).to.not.emit(tkg, "Claimed")
      expect(poapMock.smocked.tokenEvent.calls.length).to.be.equal(1)
    })

    it("should change token balance of COLLECT_AMOUNT", async () => {
      poapMock.smocked.tokenEvent.will.return.with("4510")
      expect(() => tkg.connect(player0).claim("607748")).to.changeTokenBalance(tkg, player0, "1000000000000000000")
    })

    describe("claimedBy()", () => {

      it("should return false when is not claimed", async () => {
        expect(await tkg.connect(player0).claimedBy(player0.address)).to.be.equal(false)
      })

      it("should return true when is claimed", async () => {
        poapMock.smocked.tokenEvent.will.return.with("4510")
        await tkg.connect(player0).claim("607748")
        expect(await tkg.connect(player0).claimedBy(player0.address)).to.be.equal(true)
      })

    })

  })

  describe("play()", () => {

    it("requires that sender has at sufficient funds to play", async () => {
      expect(tkg.connect(player0).play("9999999999999999999")).to.be.revertedWith("Insufficient funds to play")
    })

    describe("when the last two digits of the block number and the last two digits of the amount are different", () => {

      it("should emit Played event with miss (0) result", async () => {
        tkg.transfer(player0.address, "1000")
        expect(await tkg.connect(player0).play("42")).to.emit(tkg, "Played").withArgs(player0.address, "42", 0) // miss
      })

    })

    describe("when the last two digits of the block number and the last two digits of the amount are the same", () => {

      it("should emit Played event with hit (1) result", async () => {
        tkg.transfer(player0.address, "1000000000000000")
        const amount = await ethers.provider.getBlockNumber() + 2
        expect(await tkg.connect(player0).play(amount)).to.emit(tkg, "Played").withArgs(player0.address, amount, 1) // hit
      })

      describe("when player founded before play", () => {

        it("should increase higherAmount by founded amount", async () => {
          tkg.transfer(player0.address, "1000000000000000")
          await tkg.connect(player0).fund("42")
          expect(await tkg.higherAmount()).to.equal("0")
          const amount = await ethers.provider.getBlockNumber() + 1
          await tkg.connect(player0).play(amount)
          expect(await tkg.higherAmount()).to.be.equal(amount + 42)
        })

      })

    })

  })

})
