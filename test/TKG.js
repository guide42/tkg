const { expect } = require("chai")

describe("TKG", () => {
  let tkg, creator, player0

  beforeEach(async () => {
    const TKG = await hre.ethers.getContractFactory("TKG")

    tkg = await TKG.deploy("1000000000000000000")

    ;[
      creator,
      player0
    ] = await hre.ethers.getSigners()

    await tkg.deployed()
  })

  it("totalSupply should be 1000000000000000000", async () => {
    expect(await tkg.totalSupply()).to.equal("1000000000000000000")
  })

  it("initial TKG address balance should be 1000000000000000000", async () => {
    expect(await tkg.balanceOf(creator.address)).to.equal("1000000000000000000")
  })

  it("initial head address should be TKG address", async () => {
    expect(await tkg.head()).to.equal(creator.address)
  })

  it("initial higherAmount should be zero", async () => {
    expect(await tkg.higherAmount()).to.equal(0)
  })

  describe("fund()", () => {

    it("requires less than FUND_CAP amount to fund", async () => {
      expect(tkg.fund("9999999999999999999")).to.be.revertedWith("Fund less TKG to the head")
    })

    it("requires sender address not to be the head", async () => {
      expect(tkg.fund("42")).to.be.revertedWith("Already head")
    })

    it("should transfer amount from sender to head", async () => {
      tkg.transfer(player0.address, "1000")
      const initialHeadBalance = await tkg.balanceOf(await tkg.head())
      await tkg.connect(player0).fund("42")
      expect(await tkg.balanceOf(await tkg.head())).to.equal(initialHeadBalance.add("42"))
    })

    it("should change token balance of minus the amount", async () => {
      tkg.transfer(player0.address, "1000")
      expect(() => tkg.connect(player0).fund("42")).to.changeTokenBalance(tkg, player0, "-42")
    })

    describe("fundedOf()", () => {
      it("should add funded amount to sender", async () => {
        tkg.transfer(player0.address, "1000")
        await tkg.connect(player0).fund("42")
        expect(await tkg.fundedOf(player0.address)).to.equal("42")
      })
    })

    it("should emit Funded event", async () => {
      tkg.transfer(player0.address, "1000")
      await expect(tkg.connect(player0).fund("42")).to.emit(tkg, "Funded").withArgs(creator.address, player0.address, "42")
    })

  })

  describe("play()", () => {

    it("requires sender address not to be the head", async () => {
      expect(tkg.fund("42")).to.be.revertedWith("Already head")
    })

    describe("when the last two digits of the block number and the last two digits of the amount are different", () => {

      it("should transfer amount from sender to head", async () => {
        tkg.transfer(player0.address, "1000")
        const initialHeadBalance = await tkg.balanceOf(creator.address)
        await tkg.connect(player0).play("42")
        expect(await tkg.balanceOf(creator.address)).to.equal(initialHeadBalance.add("42"))
      })

    })

    describe("when the last two digits of the block number and the last two digits of the amount are the same", () => {

      it("should change the head to the sender", async () => {
        tkg.transfer(player0.address, "1000")
        expect(await tkg.head()).to.equal(creator.address)
        const amount = await ethers.provider.getBlockNumber() + 1
        // adding one because connect takes one block?
        await tkg.connect(player0).play(amount)
        expect(await tkg.head()).to.equal(player0.address)
      })

      it("should change the higherAmount on amount", async () => {
        tkg.transfer(player0.address, "1000")
        expect(await tkg.higherAmount()).to.equal("0")
        const amount = await ethers.provider.getBlockNumber() + 1
        // adding one because connect takes one block?
        await tkg.connect(player0).play(amount)
        expect(await tkg.higherAmount()).to.equal(amount)
      })

      it("should transfer amount from head to sender", async () => {
        const initialPlayerBalance = 1000
        tkg.transfer(player0.address, initialPlayerBalance)
        const initialHeadBalance = await tkg.balanceOf(creator.address)
        const amount = await ethers.provider.getBlockNumber() + 1
        // adding one because connect takes one block?
        await tkg.connect(player0).play(amount)
        expect(await tkg.balanceOf(player0.address)).to.equal(initialPlayerBalance + amount)
        expect(await tkg.balanceOf(creator.address)).to.equal(initialHeadBalance.sub(amount))
      })

      describe("when player founded before play", () => {

        it("should mint founded amount", async () => {
          const initialPlayerBalance = 1000
          tkg.transfer(player0.address, initialPlayerBalance)
          const fundAmount = 42
          await tkg.connect(player0).fund(fundAmount)
          const previousPlayerBalance = await tkg.balanceOf(player0.address)
          const amount = await ethers.provider.getBlockNumber() + 1
          // adding one because connect takes one block?
          await tkg.connect(player0).play(amount)
          expect(await tkg.balanceOf(player0.address)).to.equal(previousPlayerBalance.add(amount).add(fundAmount))
        })

      })

    })

  })

})
