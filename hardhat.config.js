require("@nomiclabs/hardhat-ethers")
require("@nomiclabs/hardhat-waffle")
require("@nomiclabs/hardhat-etherscan")
require("hardhat-abi-exporter")

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
const config = {
  solidity: "0.8.4",
  settings: {
    outputSelection: {
      "*": {
        "*": ["storageLayout"]
      }
    }
  },
  abiExporter: {
    flat: true,
    only: ["TKGv2"]
  }
}

if (require("fs").existsSync("./secrets.json")) {
  const { infuraId, alchemyKey, etherscanApiKey, mnemonic } = require("./secrets.json")

  module.exports = {
    ...config,
    networks: {
      rinkeby: {
        url: `https://rinkeby.infura.io/v3/${infuraId}`,
        accounts: {
          mnemonic
        }
      },
      hardhat: {
        forking: {
          url: `https://eth-mainnet.alchemyapi.io/v2/${alchemyKey}`,
          blockNumber: 13019328
        },
        accounts: {
          mnemonic
        }
      }
    },
    etherscan: {
      apiKey: etherscanApiKey
    }
  }
} else {
  module.exports = {
    ...config
  }
}
