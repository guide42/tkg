// SPDX-License-Identifier: ISC
pragma solidity ^0.8.4;

import "hardhat/console.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

contract TKG is ERC20Burnable {
  uint256 constant FUND_CAP = 1337;

  address public head;
  uint256 public higherAmount;

  mapping(address => uint256) private _funded;

  constructor(uint256 initialSupply) ERC20("Token Hackaton Guita", "TKG") {
    _mint(msg.sender, initialSupply);
    head = msg.sender;
    higherAmount = 0;
  }

  event Funded(address indexed head, address indexed account, uint256 amount);

  function fund(uint256 amount) external payable {
    require(amount < FUND_CAP, "Fund less TKG to the head");
    require(msg.sender != head, "Already head");
    _transfer(msg.sender, head, amount);
    _funded[msg.sender] += amount;
    emit Funded(head, msg.sender, amount);
  }

  function fundedOf(address account) public view virtual returns (uint256) {
    return _funded[account];
  }

  function play(uint256 amount) external payable {
    require(msg.sender != head, "Already head");
    require(amount > higherAmount, "Play at least higherAmount() TKG to win");
    uint8 lastTwoAmount = uint8(amount % 100);
    uint8 lastTwoBlock = uint8(block.number % 100);
    if (lastTwoAmount != lastTwoBlock) {
      _transfer(msg.sender, head, amount);
    } else {
      _transfer(head, msg.sender, amount);
      if (_funded[msg.sender] > 0) {
        _mint(msg.sender, _funded[msg.sender]);
        delete _funded[msg.sender];
      }
      head = msg.sender;
      if (amount > higherAmount) {
        higherAmount = amount;
      }
    }
  }
}
