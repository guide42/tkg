// SPDX-License-Identifier: ISC
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

interface Poap {
  function tokenEvent(uint256 tokenId) external returns (uint256);
}

contract TKGv2 is ERC20Burnable {
  uint256 constant FUND_CAP = 1337000000000000000000;
  uint256 constant COLLECT_AMOUNT = 1000000000000000000;

  address public head;
  uint256 public higherAmount;

  enum PlayResult { miss, hit }

  mapping(address => uint256) private _funded;
  mapping(address => bool) private _claimed;

  Poap private _poapToken;
  uint256 public poapEventId;

  constructor(uint256 initialSupply, address poapAddress, uint256 _poapEventId)
    ERC20("Token Hackaton Guita", "TKG")
  {
    _mint(msg.sender, initialSupply);
    head = msg.sender;
    higherAmount = 0;
    _poapToken = Poap(poapAddress);
    poapEventId = _poapEventId;
  }

  event Claimed(address indexed account, uint256 eventId, uint256 tokenId);

  function claim(uint256 tokenId) external payable {
    uint256 eventId = _poapToken.tokenEvent(tokenId);
    require(eventId == poapEventId, "Hold the correct POAP");
    if (_claimed[msg.sender] != true) {
      _mint(msg.sender, COLLECT_AMOUNT);
      _claimed[msg.sender] = true;
      emit Claimed(msg.sender, eventId, tokenId);
    }
  }

  function claimedBy(address account) public view virtual returns (bool) {
    return _claimed[account];
  }

  event Funded(address indexed head, address indexed account, uint256 amount);

  function fund(uint256 amount) external payable {
    require(amount < FUND_CAP, "Fund less TKG to the head");
    require(msg.sender != head, "Already head");
    _transfer(msg.sender, head, amount);
    _funded[msg.sender] += amount;
    emit Funded(head, msg.sender, amount);
  }

  function fundedOf(address account) public view virtual returns (uint256) {
    return _funded[account];
  }

  event Played(address indexed player, uint256 amount, PlayResult result);

  function play(uint256 amount) external payable {
    require(msg.sender != head, "Already head");
    require(amount > higherAmount, "Play at least higherAmount() TKG");
    require(amount <= balanceOf(msg.sender), "Insufficient funds to play");
    uint8 lastTwoAmount = uint8(amount % 100);
    uint8 lastTwoBlock = uint8(block.number % 100);
    if (lastTwoAmount != lastTwoBlock) {
      _transfer(msg.sender, head, amount);
      emit Played(msg.sender, amount, PlayResult.miss);
    } else {
      _transfer(head, msg.sender, amount);
      if (_funded[msg.sender] > 0) {
        _mint(msg.sender, _funded[msg.sender]);
        amount += _funded[msg.sender];
        delete _funded[msg.sender];
      }
      head = msg.sender;
      if (amount > higherAmount) {
        higherAmount = amount;
      }
      emit Played(msg.sender, amount, PlayResult.hit);
    }
  }
}
