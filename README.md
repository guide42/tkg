# Token Hackaton Guita

TKG is an ERC-20 with a game.

## Development

```sh
# Create your secrets
$ cp secrets.example.json secrets.json

# Run node
$ yarn hardhat node

# Deploy
$ yarn hardhat run --network localhost scripts/deploy-tkg.js

# Test
$ yarn hardhat test

# Run app
$ yarn rollup --config --watch --environment APP_ENV:development
```

## Contracts

### Rinkeby v0.1.0

```
0x89995964f0cFA484283d556552D5484e67A742CE
```

### Rinkeby v0.2.0

```
0x5b644e513614E0aFD71f91b457d08D4C3b8856B4
```

- Addresses with POAP a `tokenId` from `eventId` 4510 can use function `claim(uint256 tokenId)` to receive newly
  minted 1000000000000000000 TKG.

- All funded amount will be included in `higherAmount`.

- Verified.
  ```
  yarn hardhat verify --network rinkeby 0x5b644e513614E0aFD71f91b457d08D4C3b8856B4 1000000000000000000 0x22c1f6050e56d2876009903609a2cc3fef83b415 4510
  ```
